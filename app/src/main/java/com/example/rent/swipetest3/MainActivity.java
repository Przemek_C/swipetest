package com.example.rent.swipetest3;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    CustomAdapterOne customAdapterOne;
    CustomAdapterTwo customAdapterTwo;
    CustomAdapterTree customAdapterTree;
    ViewPager viewPagerOne;
    ViewPager viewPagerTwo;
    ViewPager viewPagerTree;

    int[] items = {R.drawable.dog, R.drawable.gwiazda, R.drawable.wolf};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CustomAdapter();
    }

    private void CustomAdapter() {
        customAdapterOne = new CustomAdapterOne(MainActivity.this, items);
        customAdapterTwo = new CustomAdapterTwo(MainActivity.this, items);
        customAdapterTree = new CustomAdapterTree(MainActivity.this, items);
        viewPagerOne = (ViewPager) findViewById(R.id.viewPagerOne);
        viewPagerTwo = (ViewPager) findViewById(R.id.viewPagerTwo);
        viewPagerTree = (ViewPager) findViewById(R.id.viewPagerTree);
        viewPagerOne.setAdapter(customAdapterOne);
        viewPagerTwo.setAdapter(customAdapterTwo);
        viewPagerTree.setAdapter(customAdapterTree);
    }
}
